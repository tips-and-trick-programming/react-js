3. Component Statefull

# Penulisan component statefull
```jsx
import React, {Component} from 'react';

import MyComp from '../../component/MyComp/MyComp';

import './MyContainer.css';

class MyContainer extends Components {
	render(){
		return (
			<div>
				<p>Ini adalah contoh container</p>
				<hr />
				<MyComp teks="Teks modifikasi" />
				<MyComp />
			</div>
		)
	}
}

export default MyContainer;
```

# State
```jsx
...
class MyContainer extends Components {
	state = {
		number: 1
	}
	
	_handleAddNumber = () => {
		this.setState({
			number: this.state.number + 1
		})
	}
	
	render(){
		return (
			<div>
				<p>{this.state.number}</p>
				<button type="button" onClick={this._handleAddNumber}>Click Me!</button>
			</div>
		)
	}
}
...
```
>`this.state.number` untuk menampilkan state
>`this.setState({number: this.state.number + 1})` untuk merubah nilai dari state
