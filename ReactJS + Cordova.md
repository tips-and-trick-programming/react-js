ReactJS + Cordova

[Sumber](https://medium.com/@pshubham/using-react-with-cordova-f235de698cc3)

# ReactJS + Cordova
1. Buat project cordova

	```
	cordova create app_perijinan id.my.giksworld.webperijinan AppPerijinan
	```
	> Akan membuat project dengan nama `AppPerijinan` pada folder `app_perijinan` dengan domain `id.my.giksworld.webperijinan`

2. Buat project reactjs

	```
	npx create-react-app app_perijinan_react
	```

3. Gabungkan kedua project tersebut
	1. Copy src/ dan public/ pada folder project react ke folder project cordova
	2. Copy “scripts”, “dependencies” dan “browserList” pada file package.json di folder project react ke file package.json di folder project cordova
	
	```json
	{
	  "homepage": "./",
	  "name": "id.my.giksworld.webperijinan",
	  "displayName": "AppPerijinan",
	  "version": "1.0.0",
	  "description": "A sample Apache Cordova application that responds to the deviceready event.",
	  "main": "index.js",
	  "scripts": {
	    "start": "react-scripts start",
	    "build": "react-scripts build",
	    "test": "react-scripts test",
	    "eject": "react-scripts eject"
	  },
	  "dependencies": {
	    "@testing-library/jest-dom": "^4.2.4",
	    "@testing-library/react": "^9.3.2",
	    "@testing-library/user-event": "^7.1.2",
	    "react": "^16.13.1",
	    "react-dom": "^16.13.1",
	    "react-scripts": "3.4.3"
	  },
	  "browserslist": {
	    "production": [
	      ">0.2%",
	      "not dead",
	      "not op_mini all"
	    ],
	    "development": [
	      "last 1 chrome version",
	      "last 1 firefox version",
	      "last 1 safari version"
	    ]
	  },
	  "keywords": [
	    "ecosystem:cordova"
	  ],
	  "author": "Apache Cordova Team",
	  "license": "Apache-2.0",
	  "devDependencies": {
	    "cordova-plugin-whitelist": "^1.3.4"
	  },
	  "cordova": {
	    "plugins": {
	      "cordova-plugin-whitelist": {}
	    }
	  }
	}
	```
	![Screenshot from 2020-09-07 20-59-43.png](../_resources/34a80a97b35e43e8a509774fc3726653.png)

4. Update dependency pada project cordova

	```
	cd app_perijinan
	yarn install
	```

> Sekarang project yang akan dipakai seterusnya adalah project cordova

5. Tambahkan tag berikut pada bagian `<head>` dari file `public/index.html`

	```html
	<meta http-equiv="Content-Security-Policy" content="default-src 'self' data: gap: https://ssl.gstatic.com 'unsafe-eval' 'unsafe-inline'; style-src 'self' 'unsafe-inline'; media-src *; img-src 'self' data: content:;">
	<meta name="format-detection" content="telephone=no">
	<meta name="msapplication-tap-highlight" content="no">
	<meta name="viewport" content="initial-scale=1, width=device-width, viewport-fit=cover">
	```

6. Dan tambahkan tag berikut tepat sebelum tag `</body>` pada file `public/index.html`

	```html
	<script src="cordova.js" type="text/javascript"></script>
	```

7. Ubah file `src/index.js` menjadi sebagai berikut

	```js
	import React from 'react';
	import ReactDOM from 'react-dom';
	import './index.css';
	import App from './App';
	import * as serviceWorker from './serviceWorker';
	
	const renderReactDom = () => {
	  ReactDOM.render(<App />, document.getElementById('root'));
	};
	
	if (window.cordova) {
	  document.addEventListener('deviceready', () => {
	    renderReactDom();
	  }, false);
	} else {
	  renderReactDom();
	}
	
	// If you want your app to work offline and load faster, you can change
	// unregister() to register() below. Note this comes with some pitfalls.
	// Learn more about service workers: https://bit.ly/CRA-PWA
	serviceWorker.unregister();
	```

8. Tambahkan script berikut pada file `config.xml` tepat dibawah tag `<widget>`

	```xml
	<hook type="before_prepare" src="scripts/prebuild.js" />
	```

9. Install package rimraf

	```
	yarn add rimraf
	```

10. Buat file `scripts/prebuild.js`

	```js
	const path = require('path');
	const { exec } = require('child_process');
	const fs = require('fs');
	const rimraf = require('rimraf');
	
	function renameOutputFolder(buildFolderPath, outputFolderPath) {
	  return new Promise((resolve, reject) => {
	    fs.rename(buildFolderPath, outputFolderPath, (err) => {
	      if (err) {
	        reject(err);
	      } else {
	        resolve('Successfully built!');
	      }
	    });
	  });
	}
	
	function execPostReactBuild(buildFolderPath, outputFolderPath) {
	  return new Promise((resolve, reject) => {
	    if (fs.existsSync(buildFolderPath)) {
	      if (fs.existsSync(outputFolderPath)) {
	        rimraf(outputFolderPath, (err) => {
	          if (err) {
	            reject(err);
	            return;
	          }
	          renameOutputFolder(buildFolderPath, outputFolderPath)
	            .then(val => resolve(val))
	            .catch(e => reject(e));
	        });
	      } else {
	        renameOutputFolder(buildFolderPath, outputFolderPath)
	          .then(val => resolve(val))
	          .catch(e => reject(e));
	      }
	    } else {
	      reject(new Error('build folder does not exist'));
	    }
	  });
	}
	
	module.exports = () => {
	  const projectPath = path.resolve(process.cwd(), './node_modules/.bin/react-scripts');
	  return new Promise((resolve, reject) => {
	    exec(`${projectPath} build`,
	      (error) => {
	        if (error) {
	          console.error(error);
	          reject(error);
	          return;
	        }
	        execPostReactBuild(path.resolve(__dirname, '../build/'), path.join(__dirname, '../www/'))
	          .then((s) => {
	            console.log(s);
	            resolve(s);
	          })
	          .catch((e) => {
	            console.error(e);
	            reject(e);
	          });
	      });
	  });
	};
	```

11. Ready to go!

# Tips

> If you are planning to use react-router-dom for routing in the app, use `<HashRouter>` instead of `<BrowserRouter>`

> While accessing the cordova object access it using the window object, or else the react build would throw an undefined variable exception. For .e.g if you are using the local notification plugin ( [plugin link](https://github.com/katzer/cordova-plugin-local-notifications) ), instead of using it as:

```js
cordova.plugins.notification.local.schedule({
    ....
});
```

> access it using the window object:

```js
window.cordova.plugins.notification.local.schedule({
    ....
});
```

