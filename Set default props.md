Set default props

# Mengatur nilai default dari suatu component
```jsx
MyComp.defaultProps = {
	teks: "Hello world"
}
```
Pada contoh diatas maka props `teks` dari component `MyComp` akan diisi dengan nilai default `"Hello world"`
Mengatur nilai default berguna saat kita memanggil suatu component namun tidak mengoper beberapa props yang tidak diperlukan sehingga tidak mengganggu fungsional dari component yang dipanggil