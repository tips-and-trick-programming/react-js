7.2. Redux Struktur Folder

# Rekomendasi Struktur Folder

> Buat reducer dan action type pada file terpisah, agar file `index.js` tidak terlalu banyak berisi code
> Pembuatan `globalActionType.js` juga memudahkan kita untuk memakai fungsional dispatch pada suatu component, dengan kata lain untuk meminimalisir kesalahan pengetikan action type nya

```
- src
  - redux
    - reducer
      - globalActionType.js
      - globalReducer.js
```

`globalActionType.js`

```jsx
const ActionType = {
	INCREMENT: 'INCREMENT',
	DECREMENT: 'DECREMENT',
	CHANGE_VALUE: 'CHANGE_VALUE'
}

export default ActionType;
```

`globalReducer.js`

```jsx
import ActionType from './globalActionType'

const initialState = {
  counter: 0
}

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.INCREMENT:
      return {
        ...state,
        counter: state.counter + 1
      }
    case ActionType.DECREMENT:
      return {
        ...state,
        counter: state.counter + 1
      }
    case ActionType.CHANGE_VALUE:
      return {
        ...state,
        counter: action.newValue
      }
    default:
      return state
  }
}

export default rootReducer;
```