Arrow function

# Kegunaan arrow function
Di dalam react arrow function memiliki kegunaan teresendiri saat digunakan dalam satefull component.
```jsx
...
_myfunc () {
	console.log(this);  // menampilkan null karena di dalam function tidak ada variable yang diatur
}

_myfunc = () => {
	console.log(this);  // menampilkan semua parameter dari class nya, seperti state, function, dll
}
...
```

>Arrow function akan merujuk `this` pada class nya, sedangkan function biasa akan merujuk `this` pada function itu sendiri
>Jadi untuk menggunakan fungsional dari `React.Component` maka harus menggunakan arrow function, contohnya adalah `setState`