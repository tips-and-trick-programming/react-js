Handling Form

# Salah satu cara terbaik untuk menghandle form adalah sebagai berikut
```jsx
...
  constructor(props) {
    super(props)

	// Untuk data yang akan ditampilkan gunakan array, untuk penggunaan map
	// Untuk data yang akan dikirim gunakan array object untuk memudahkan pengiriman data
    this.state = {
      post: [],
      formPost: {
        id: "1",
        title: "",
        body: ""
      }
    }
  }
  
  // event berisi informasi lengkap dari form
  handleFormChange = (event) => {
	// Clone state yang akan dirubah
    let cloneFormPost = {...this.state.formPost}
    
    // Rubah value nya ambil dari event
    cloneFormPost[event.target.name] = event.target.value

	// setState menjadi cloningan yang dimaksud
    this.setState({
      formPost: cloneFormPost
    },
    () => {
      console.log(this.state.formPost)
    })
  }
  
  render() {
    return (
      <Fragment>
        <div>
          {/* Gunakan nama form input sama dengan state dan nama kolom database untuk memudahkan pengiriman data */}
          <input type="text" name="title" onChange={this.handleFormChange} /><br/>
          <input type="text" name="body" onChange={this.handleFormChange} /><br/>
          <button>SUBMIT</button>
        </div>
      </Fragment>
    )
  }
...
```

> `...variable` adalah spread operator, digunakan untuk mengulang value dari variable yang bersifat iterable seperti array
> [Sumber 1](https://www.freecodecamp.org/news/an-introduction-to-spread-syntax-in-javascript-fba39595922c/) [Sumber 2](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax)




