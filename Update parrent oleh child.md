Update parrent oleh child

# Mengirimkan props bertipe function
1. Component parrent
```jsx
...
import Child from '../Child/Child';

class Parrent extends Component {
	state = {
		number: 0
	}
	
	_handleCounterChange = (newValue) => {
		this.setState({
			number: newValue
		})
	}
	
	render(){
		return(
			<div>
			<p>{this.state.number}</p>
			<Child onCounterChange={(value) => this._handleCounterChange(value)} />  // Props dapat berupa function seperti ini
			</div>
		)
	}
}
...
```
2. Component child
```jsx
...
class Child extends Component {
	state = {
		number: 0
	}
	
	_handleCounterChange = (newValue) => {
		this.props.onCounterChange(newValue)              // Panggil props untuk mengubah value pada parrent
	}
	
	_handleAddNumber = () => {
		this.setState({
			number: this.state.number + 1
		}, () => {
			this._handleCounterChange(this.state.number)  // Panggil _handleCounterChange saat state number sudah berhasil berubah
		})
	}
	
	render(){
		return (
			<div>
				<button type="button" onClick={this._handleAddNumber}>Click Me!</button>
			</div>
		)
	}
}
...
```
> Props dapat berupa function sehingga valid jika menuliskan `onCounterChange={(value) => this._handleCounterChange(value)`

> Tips ini sangat berguna saat ingin mengupdate kondisi pada parrent melalui child
> cont. saat klik tombol `Tambah ke Keranjang` pada component child, component parrent pada icon keranjang akan berubah nilai badgesnya menyesuaikan dengan banyak barang yang dipilih